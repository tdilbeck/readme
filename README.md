## Ty Dilbeck's README

**Ty Dilbeck, [Senior Manager, Security Risk](https://handbook.gitlab.com/job-families/security/security-leadership/#senior-manager-security)**

This page is intended to help folks understand what it might be like to work with me. It’s also a well-intentioned effort to build trust through vulnerability and transparency.

## About me

* I live in Nashville, TN with my wife (Caroline) and two boys (Tommy - 2019 & Fitz - 2021)
* I grew up in Montgomery, Alabama and graduated from The University of Alabama majoring in Management Information Systems with a minor in Creative Writing. My experience at Bama MIS helped shape the way that I approach problem solving. 
* Long term goal setting has always been a part of my life.
* I have been rock climbing (primarily bouldering) since 2009 and find that the physical and mental challenge associated with the effort helps focus my mind and reduce work-related stress/anxiety. I met Caroline at the climbing gym in 2011. Injuries include damaged tendons in fingers, torn hamstring, pes anserine bursitis, tendonitis and DIP synovitis. Frequent injuries have challenged me mentally and made me more resilient both in climbing and in my career. I am currently focused on progression on the [Moonboard](https://www.moonboard.com/what-is-the-moonboard) and love the fact it allows me to track my growth over time.
* Caroline and I started investing in residential real estate in Nashville in 2011. Caroline manages our properties and we are constantly looking to expand our portfolio. I have started every morning with a quick 5 min house search since we got started. We also love budgeting and working toward shared goals. Our investment approach has been shaped by the [FIRE movement](https://www.mrmoneymustache.com/2013/02/22/getting-rich-from-zero-to-hero-in-one-blog-post/) and the advice of [Jack Bogle](https://en.wikipedia.org/wiki/John_C._Bogle). 

## My Top Strengths

* Growth Mindset: I enjoy challenging myself and grow uncomfortable/bored when I am not regularly challenged. As I have gotten older, I have become better at challenging myself to grow as an individual and a leader.  I actively encourage and support team members who push themselves outside their comfort zones and take ownership of their professional development.
* Strategic Thinking: I think 3-4 steps ahead and like to have a vision of end state before taking action. I am goal oriented.
* Efficiency: I try to find the shortest path from point A to B without sacrificing quality. 
* Problem Solving: I have learned to channel my natural attention to detail into helping teams identify strategic opportunities and risks. My analytical mindset helps me anticipate challenges and guide teams toward effective solutions while giving them space to develop their own approaches.
* Prioritization: I enjoy regularly stepping back from work to ensure I am focused on tasks that are supporting both short and long term objectives. 
* [Psych](https://www.britannica.com/dictionary/psych): I try to bring a positive attitude to work. I believe psych is contagious.


## My working style

* Like my coffee, I do my best work in the mornings. 
* I do not mind jumping in and getting my hands dirty. 
* "Why?" is a question I ask regularly to ensure my actions align with my goals and objectives. I purpose to understand the rationale behind the decisions I am making and ask others to share their rationale when they make decisions that impact me.
* I love completing deliverables (results) both as an individual and as a team. If I don't feel like I can deliver results I try to step back and take stock of the situation so that I can get on-track. (Related: Enneagram result of Self-Preservation Type 3)
* I try to anticipate the needs of my team proactively. I value independent problem solving.
* I am practicing active listening and asking questions to understand different perspectives before moving to solutions. This includes regular check-ins that focus on removing obstacles rather than directing solutions.
* I enjoy creating a vision and building a roadmap to execute on this vision. I am at my best when I am aligned with my team on our goals and am given the space needed to execute and report.
* I like to celebrate accomplishments. This can be as simple as treating myself to lunch, a quick workout or leaving work a few minutes early to spend time with family. Acknowledging a job well done brings a lot of joy.
* I start each day by building out my to-do list and reviewing the status of the tasks I hope to accomplish this week and next. This enables me to better prioritize my work and helps ensure I am aligned with my team and with leadership. 
* I am good at negotiating deliverables/milestones when priorities shift. This is a self-preservation tactic that helps me reduce anxiety around changes to my own plans so that I can execute.
* I appreciate GitLab's commitment to documentation and transparency and generally prefer problem solving in a Slack Team Channel over a synchronous meeting.
* I believe in excellent and professional customer service. It is important to me that our stakeholders have positive interactions with me and my team and leave feeling like they understand the value we add.
* Response time SLAs during working hours:
   * Slack DMs: within the hour
   * Slack channels mentions: within the hour
   * GitLab @mentions: within 1 day
   * Email: within 1-2 working days
* I value my time off work to recharge and rarely check any work messages outside of my working hours. 
* I do not have Slack on my phone.

## What I assume about others

* I will assume that you'll follow through on  agreements we've made and that you'll communicate any changes in priorities. This includes adjustments to agreed upon due dates.
* In general, [GitLab's CREDIT values](https://about.gitlab.com/handbook/values/) and the sub-values strongly resonate with me. I will assume that you will follow them.

## Areas of Growth

I am actively working on my leadership journey in several areas:

* Transitioning from tactical problem-solving to strategic leadership, including learning to find comfort in giving teams more autonomy while maintaining appropriate oversight
* Problem Solving: I am quick to begin solutioning when team members bring me a problem. This can leave folks not feeling heard or trusted to solution on their own. I am learning the value of listening and asking questions.
* When I do not feel aligned with others or do not see a clear path forward I can get frustrated. Most of the time, a quick workout or a good night's sleep will provide the time I need to gain perspective.

## My management style

I became a people manager in 2015 and my approach continues to evolve with experience and feedback. In 2024, I began meeting with a Career Coach to learn more about my strengths and areas of opportunity. As an outcome, my two areas of growth include listening to and trusting others. In 2025, I am making a concerted effort to provide my teammembers with more independence and to focus my efforts on identifying new opportunities for cross-departmental collaboration. Specifically, I intend to improve my active listening by asking questions and refraining from solutioning unless asked. I also intend to spend more time building relationships with leaders inside of GitLab and within the industry to create opportunities for the team to add greater value.

* I am learning to embrace productive discomfort as teams take on more ownership of their projects.
* I value creating space for team members to develop their own solutions before offering guidance
* My direct reports can easily reach me via Slack for anything that needs a quick response. Discussion topics are shared via our 1-1 agenda document and discussed synchronously. 
* It is my objective to support my team so that folks can take on additional responsibility.  I find that the team is at its best when folks feel empowered to stretch themselves. This in turn provides me with the opportunity to do the same.
* I purpose to deliver on-the-job feedback in real time. My formal performance feedback is simply a summary of past conversations.

## Resources and Personality Tests 

### 2021 MBTI Executive Type ([ Architect (INTJ-A)](https://www.16personalities.com/intj-personality?utm_source=email&utm_medium=welcome-architect&utm_campaign=description))

An Architect (INTJ) is a person with the Introverted, Intuitive, Thinking, and Judging personality traits. These thoughtful tacticians love perfecting the details of life, applying creativity and rationality to everything they do. Their inner world is often a private, complex one.

###  Enneagram Result: The Achiever

Threes are self-assured, attractive, and charming. Ambitious, competent, and energetic, they can also be status-conscious and highly driven for advancement. They are diplomatic and poised, but can also be overly concerned with their image and what others think of them. They typically have problems with workaholism and competitiveness. At their Best: self-accepting, authentic, everything they seem to be—role models who inspire others. ([Source](https://www.enneagraminstitute.com/type-3))

### September 2023 [360 Feedback](https://drive.google.com/file/d/19ThDUfkMCxa-FtNTwY-OpAcD1NTnXK_f/view?usp=drive_link)
